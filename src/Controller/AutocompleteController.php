<?php

namespace Drupal\linked_data_field\Controller;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Controller\ControllerBase;
use Drupal\linked_data_field\Entity\LinkedDataEndpointInterface;
use Drupal\linked_data_field\LDLookupServiceInterface;
use Drupal\linked_data_field\Plugin\LinkedDataEndpointTypePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AutocompleteController.
 */
class AutocompleteController extends ControllerBase {

  /**
   * Service to do LC Subject lookups.
   *
   * @var Drupal\linked_data_field\LDLookupServiceInterface
   */
  protected $ldLookup;

  /**
   * The plugin manager for endpoint types.
   *
   * @var \Drupal\linked_data_field\Plugin\LinkedDataEndpointTypePluginManager
   */
  protected $ldEntityTypePluginManager;

  /**
   * AutocompleteController constructor.
   *
   * @param Drupal\linked_data_field\LDLookupServiceInterface $ld_lookup_service
   *   The lookup service to query against.
   */
  public function __construct(LinkedDataEndpointTypePluginManager $ld_entity_type_plugin_manager) {
    $this->ldEntityTypePluginManager = $ld_entity_type_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.linked_data_endpoint_type_plugin')
    );
  }

  /**
   * Autocomplete controller.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The page request object.
   *
   * @return string
   *   Return Autocomplete result string.
   */
  public function handleAutocomplete(LinkedDataEndpointInterface $linked_data_endpoint, Request $request) {
    $results = [];
    $endpoint_id = array_slice(explode('/', $request->getPathInfo()), -1)[0];
    $endpoint = $this->entityTypeManager()->getStorage('linked_data_endpoint')->load($endpoint_id);
    $plugin = $this->ldEntityTypePluginManager->createInstance($endpoint->get('type'), ['endpoint' => $endpoint]);

    $debug = $request->query->get('_ldquery_debug');

    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = mb_strtolower(array_pop($typed_string));

      $service_results = $plugin->getSuggestions($typed_string, $debug);

      $results = $plugin->postProcessResponse($service_results);

      $serialized_results = [];
      // Hack - convert the label and value into a serialized response
      // because the AJAX callback only gets the value part.
      foreach ($results as $result){
        $serialized_results[] = ['label' => $result['label'],
        'value' => $result['label'] . ' - [[' . $result['value'] . ']]'  ];
      }

      return new JsonResponse($serialized_results);
    }
  }

}
