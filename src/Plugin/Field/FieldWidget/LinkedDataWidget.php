<?php

namespace Drupal\linked_data_field\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'grid_widget' widget.
 *
 * @FieldWidget(
 *   id = "linked_data_widget",
 *   label = @Translation("Linked Data Lookup Widget"),
 *   field_types = {
 *     "linked_data_field"
 *   }
 * )
 */
class LinkedDataWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $source = $items->getSetting('source');

$element_id = $items->getFieldDefinition()->getName() . '-' . $delta;

    $element['value'] = $element + [
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
        '#description' => $this->t('Autocomplete field'),
        '#maxlength' => 200,
        '#prefix' => '<div id="' . $element_id .'" class="field__label">' . $items->getFieldDefinition()->label() . '</div>',
        '#autocomplete_route_name' => 'linked_data_lookup.autocomplete',
        '#autocomplete_route_parameters' => ['linked_data_endpoint' => $source],
        '#size' => 200,
        '#ajax' => [
          'callback' => [$this, 'ajaxRebuild'],
          'event' => 'autocompleteclose',
          'wrapper' => $element_id,
        ],
      ];
    unset($element['value']['#title']);
    //$element['value']['#attached']['library'][] = 'linked_data_field/ld-autocomplete';

    $element['url'] = [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->url) ? $items[$delta]->url : NULL,
      '#delta' => $delta,
      '#size' => 200,
      '#prefix' => '<div class="field__label">Definition URL</div>',
      '#weight' => $element['#weight'],
      '#maxlength' => 200,
    ];

    $element['url']['#attributes']['class'][] = 'subject-url-input';

    return $element;
  }

  /**
   * AJAX callback to populate Label and URL field components.
   *
   * @param array $form
   *   The form array.
   * @param FormStateInterface $form_state
   *   The form state object.
   * @return AjaxResponse
   *   The AJAX response.
   */
  public function ajaxRebuild(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $element = $form_state->getTriggeringElement();
    $extracted_values = $this->extractLabelAndUrl($element['#value']);
    if (is_array($extracted_values)) {
      $label = $extracted_values['label'];
      $url = $extracted_values['url'];

$value_selector = 'input[data-drupal-selector="' . $element['#attributes']['data-drupal-selector'] . '"]';
$url_selector  = 'input[data-drupal-selector="' . $form[$element['#parents'][0]]['widget'][$element['#delta']]['url']['#attributes']['data-drupal-selector'] . '"]';

      $response->addCommand(new InvokeCommand($value_selector, 'val', [$label]));
      $response->addCommand(new InvokeCommand($url_selector, 'val', [$url]));
    }
    return $response;
  }

  /**
   * Parse Label and URL from autocomplete controller response.
   *
   * The response format is "Label - [[url]]".
   *
   * @param string $inputString
   *   The string to parse.
   * @return array|FALSEvoid
   *   The form element components or FALSE.
   */
  private function extractLabelAndUrl($inputString): array|FALSE {

    if (preg_match('/(.+?)\s*-\s*\[\[(.*?)\]\]/', $inputString, $matches)) {
      $label = trim($matches[1]); // Extract and trim the value
      $url = trim($matches[2]);   // Extract and trim the URL
      return array('label' => $label, 'url' => $url);

    }
    return FALSE;
  }

}
